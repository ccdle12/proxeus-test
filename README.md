# Intro
This is a sample DAPP called 'Cow Crazy!'. This DAPP is a voting based DAPP with users voting on their favorite cow.
The DAPP is deployed and hosted on Firebase. 

I would usually follow a development pattern of 
```docker -> docker-compose -> kubernetes managing containers on AWS/GKE```

but for simplicities sake, it seemed much more straightforward to deploy the front-end on Firebase as no other micro-services are needed.

# Dependencies
* Docker - For the local ganache test chain and truffle
```
 https://docs.docker.com/install/
```

* npm
```
https://www.npmjs.com/get-npm
```

## View the deployed DAPP
The deployed DAPP on Firebase is the same DAPP as in this project folder.
The DAPP deployed on Firebase is ONLY usable on the Ropsten Test Network.
```
https://proxeus-test.firebaseapp.com
```

## Running Locally
* Run ganache and truffle
```
$ docker-compose up
```

### Import seed phrase to MetaMask
```
"rookie defy drastic net eight addict you mosquito broccoli width clap burden"
```
If the account fails to make transactions due to the transaction nonce's being out of sync, go to ```Settings``` in MetaMask and click ```Reset Account```

### Migrate contracts
```
$ cd proxeus-test
$ contracts/truffle/contract-scripts/truffle migrate
```

### Run the DAPP for the Local Environment
```
$ cd ./front-end/cow-crazy
$ npm install
$ npm run dev
```

### View the DAPP
```
$ http://localhost:8080
```

## Running Ropsten DAPP
### Install http-server 
* This will allow us to spin up a server locally for our front-end and since it will be using the dist folder, this will be referencing the deployed contract on Ropsten
```
$ npm install http-server -g
```

### Build the dist folder
```
$ cd ./front-end/cow-crazy
$ npm install
$ npm run build
```

### Serve the front-end
```
$ http-server ./dist
```

### View the DAPP
```
$ http://localhost:8080
```