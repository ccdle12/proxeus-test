'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  CONTRACT_ADDRESS: '"0x5b48e3d066bd7a20ff7979ab97ec8afaec5e0221"'
})
