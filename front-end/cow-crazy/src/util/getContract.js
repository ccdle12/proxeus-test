import Web3 from 'web3'
import { address, ABI } from './abi/CowCrazy'

let getContract = new Promise(function (resolve, reject) {
  let web3 = new Web3(window.web3.currentProvider)
  let cowCrazy = web3.eth.contract(ABI).at(address)

  resolve(cowCrazy)
})

export default getContract
