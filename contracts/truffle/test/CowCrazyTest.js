const CowCrazy = artifacts.require('CowCrazy.sol')
const BigNumber = web3.BigNumber
const should = require('chai')
  .use(require('chai-as-promised'))
  .use(require('chai-bignumber')(BigNumber))
  .should()

contract('CowCrazy', function(accounts) {
  
  const deploying_account = accounts[0]
  const account1 = accounts[1]
  const account2 = accounts[2]
  const account3 = accounts[3]
  const account4 = accounts[4]

  /**
   * Helper Functions
   */
  const bigNumber = (x) => new BigNumber(x)
  const toEth = (x) => web3.fromWei(x).toNumber()
  const toWei = (x) => web3.toWei(x, 'ether')
  const sendTx = (sender, receiver, amount) => web3.eth.sendTransaction({from: sender, to: receiver, value: toWei(amount)})
  const getBalance = (x) => web3.eth.getBalance(x)
  async function helperTryCatch(fn) {
    try {
      await fn
    } catch(err) {
       return false
    } 
    return true
  }
  
  before(async() => {
    this.CowCrazy = await CowCrazy.deployed()
  })

  it("should exist", async() => {
    await this.CowCrazy.should.exist
  })

  it('should return the length of Cows array', async() => {
    let length = await this.CowCrazy.getNumberOfCows.call()
    // Should update as we add more cows
    length.toNumber().should.equal(5)
  })

  it('should return an array of votes', async() => {
    let length = await this.CowCrazy.getNumberOfCows.call()

    /** Off load responsiblity for expensive OP calls to the API or Front-end */
    allVotes = []
    for(let i = 0; i < length; i++) {
      let vote = await this.CowCrazy.getVotes.call(i)
      allVotes.push(vote)
    }

    allVotes[0][0].should.equal('JAMES')
    allVotes[0][1].toNumber().should.equal(0)

    allVotes[1][0].should.equal('TIM')
    allVotes[1][1].toNumber().should.equal(0)

    allVotes[2][0].should.equal('TILDA')
    allVotes[2][1].toNumber().should.equal(0)
  })

  it('shoud get votes according to the name of the cow', async() => {
    let votesForJames = await this.CowCrazy.getVotesForCow.call('JAMES')
    votesForJames.toNumber().should.equal(0)

    let votesForTim = await this.CowCrazy.getVotesForCow.call('TIM')
    votesForTim.toNumber().should.equal(0)

    let votesForTilda = await this.CowCrazy.getVotesForCow.call('TILDA')
    votesForTilda.toNumber().should.equal(0)
  })

  it('should revert if trying to vote for a cow that doesnt exist', async() => {
    let attempedTx = await helperTryCatch(this.CowCrazy.getVotesForCow.call('BEN'))
    attempedTx.should.be.false
  })

  it('should vote for a cow according to its name and increment vote count', async() => {
    await this.CowCrazy.voteForCow('JAMES')
    let votesForJames = await this.CowCrazy.getVotesForCow.call('JAMES')

    votesForJames.toNumber().should.equal(1)
  })

  it('should revert attempting to vote for a cow that doesnt exist', async() => {
    let attempedTx = await helperTryCatch(this.CowCrazy.voteForCow('BEN'))

    attempedTx.should.be.false
  })

  it('should vote for another cow, increment and call all votes', async() => {
    await this.CowCrazy.voteForCow('TILDA')

    let voteForTilda = await this.CowCrazy.getVotesForCow.call('TILDA')
    voteForTilda.toNumber().should.equal(1)

    let length = await this.CowCrazy.getNumberOfCows.call()

    /** Off load responsiblity for expensive OP calls to the API or Front-end */
    allVotes = []
    for(let i = 0; i < length; i++) {
      let vote = await this.CowCrazy.getVotes.call(i)
      allVotes.push(vote)
    }

    allVotes[0][0].should.equal('JAMES')
    allVotes[0][1].toNumber().should.equal(1)

    allVotes[1][0].should.equal('TIM')
    allVotes[1][1].toNumber().should.equal(0)

    allVotes[2][0].should.equal('TILDA')
    allVotes[2][1].toNumber().should.equal(1)
  })
  
})
