pragma solidity ^0.4.18;

library SafeMathLib {
    function plus(uint256 a, uint256 b) internal returns (uint256) {
        uint256 c = a + b;
        assert(c >= a);
        return c;
    }
}

contract CowCrazy {
    using SafeMathLib for uint256;

    modifier cowShouldExist(string name) {
        require(cowsMapping[name].exists);
        _;
    }

    struct Cow {
        string name;
        uint256 votes;
        bool exists;
    }

    mapping(string => Cow) cowsMapping;
    string[] public cows;

    constructor() public {
        createCow("JAMES");
        createCow("TIM");
        createCow("TILDA");
        createCow("LUCY");
        createCow("JERRY");
    }

    function createCow(string name) internal {
        Cow memory cow;
        cow.name = name;
        cow.votes = 0;
        cow.exists = true;
        cows.push(cow.name);
        cowsMapping[cow.name] = cow;
    }

    function getNumberOfCows() public returns (uint256) {
        return cows.length;
    }

    function getVotes(uint256 index) public returns (string, uint256) {
        return (cowsMapping[cows[index]].name, cowsMapping[cows[index]].votes);
    }

    function getVotesForCow(string name) cowShouldExist(name) public returns (uint256) {
        return cowsMapping[name].votes;
    }

    function voteForCow(string name) cowShouldExist(name) public {
        cowsMapping[name].votes = cowsMapping[name].votes.plus(1);
    }

}
